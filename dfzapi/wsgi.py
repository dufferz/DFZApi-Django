from dotenv import load_dotenv
load_dotenv()
# ^ added as per python-dotenv docs
"""
WSGI config for dfzapi project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dfzapi.settings')

application = get_wsgi_application()
