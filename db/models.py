from django.db import models

class Products(models.Model):
    #_id            = ObjectId
    title          = models.CharField(max_length=255)
    image          = models.CharField(max_length=255)
    description     = models.TextField()
    onSale         = models.BooleanField(default=False)
    category       = models.CharField(max_length=50)
    price          = models.IntegerField()
    salePrice      = models.IntegerField()
    supplier       = models.CharField(max_length=50)
    sku            = models.CharField(max_length=100)
    #  specs       = models.AutoField() # come back here, db contains array objects. Converrt to json and parse?
    #  reviews     = models.AutoField() # come back here, db contains array objects. Converrt to json and parse?
    #  images      = models.AutoField() # come back here, db contains array objects. Converrt to json and parse?
    stripeID       = models.CharField(max_length=50)
    stripePriceID  = models.CharField(max_length=50)
    stripeSKUID    = models.CharField(max_length=50)
    stripeID       = models.CharField(max_length=50)
    

    def __str__(self):
        return self.title

class storeOrders(models.Model):
    orderID               = models.CharField(max_length=50)
    customerID            = models.CharField(max_length=50)
    paymentID             = models.CharField(max_length=50)
    checkoutID            = models.CharField(max_length=50)
    eventID               = models.CharField(max_length=50)
    invoiceID             = models.CharField(max_length=50)
    customerName          = models.CharField(max_length=100)
    customerEmail         = models.CharField(max_length=100)
    customerPhone         = models.CharField(max_length=20) # is a number?
    paymentID             = models.CharField(max_length=50)
    #  deliveryInfo       = models.AutoField() # come back here, db contains array objects. Converrt to json and parse?
    shippingID            = models.CharField(max_length=50)
    #  items              = models.AutoField()
    orderTotalPrice       = models.IntegerField()
    paymentAmountRecieved = models.IntegerField()
    additionalNotes       = models.TextField()
    orderStatus           = models.CharField(max_length=20)
    deliveryStatus        = models.CharField(max_length=20)
    delivered             = models.BooleanField(default=False)
    paymentType           = models.CharField(max_length=20)
    refunded              = models.BooleanField(default=False)
    lastModified          = models.CharField(max_length=30)
    dateCreated           = models.DateTimeField()
    riskScore             = models.CharField(max_length=10)
    riskLevel             = models.CharField(max_length=10)

    def __str__(self):
        return super().__str__()

class Categories(models.Model):
    title           = models.CharField(max_length=30)
    image           = models.CharField(max_length=100)
    description     = models.TextField()
    category        = models.CharField(max_length=50)
    # subCategories = models.AutoField()
    fullDescription = models.TextField()
    topBanner       = models.CharField(max_length=100)

    def __str__(self):
        return super().__str__()


class Jobs(models.Model):
    firstname    = models.CharField(max_length=50)
    lastname     = models.CharField(max_length=50)
    email        = models.CharField(max_length=50)
    city         = models.CharField(max_length=50)
    district     = models.CharField(max_length=50)
    postcode     = models.CharField(max_length=50)
    date         = models.CharField(max_length=50)
    todo         = models.TextField()
    done         = models.TextField()
    #  parts     = models.AutoField # come back here
    modified     = models.DateTimeField()
    created      = models.DateTimeField()
    status       = models.CharField(max_length=50)
    model        = models.CharField(max_length=50)
    make         = models.CharField(max_length=50)
    year         = models.CharField(max_length=50)
    serial       = models.CharField(max_length=50)
    # images     = models.AutoField() # come back ere 
    labourHours  = models.CharField(max_length=50)
    assigned     = models.CharField(max_length=50)
    jobNumber    = models.CharField(max_length=50)

    def __str__(self):
        return super().__str__()

class Parts(models.Model):
    dateAdded      = models.CharField(max_length=50)
    modifed        = models.CharField(max_length=50)
    stockCount     = models.CharField(max_length=50)
    partName       = models.CharField(max_length=50)
    thumbnail      = models.CharField( max_length=50)
    stock          = models.CharField(max_length=50)
    location       = models.CharField(max_length=50)
    SKU            = models.CharField(max_length=50) # ??
    partNumber     = models.CharField(max_length=50)
    price          = models.CharField(max_length=50)
    Location       = models.CharField(max_length=50)
    addedBy        = models.CharField(max_length=50)
    showOnWebStore = models.BooleanField(default=False)
    manufacturer   = models.CharField(max_length=50)
    supplier       = models.CharField(max_length=50)
    thumbnail      = models.CharField(max_length=50)


