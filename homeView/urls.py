from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('jobs/', views.jobs, name="jobs"),
    path('categories/', views.categories, name="categories"),
    #path('storeorders/', views.storeorders, name="storeorders")

]
