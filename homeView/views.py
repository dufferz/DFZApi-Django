from django.shortcuts import render
from django.http import HttpResponse, JsonResponse

from db.models import Products, Jobs, Categories, storeOrders

# Create your views here.


def index (request):
    x = Products.objects.values()
    
    return JsonResponse({"products": list(x)})

def jobs (req):
    x = Jobs.objects.values()
    
    return JsonResponse({"jobs": list(x)})

def categories (req):
    x = Categories.objects.values()
    
    return JsonResponse({"categories": list(x)})

def storeOrders (req):
    x = storeOrders.objects.values()
    
    return JsonResponse({"storeOrders": list(x)})

